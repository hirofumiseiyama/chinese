$(document).ready(function(){
  /*---------------------------------------
   * 設定
   ---------------------------------------*/
  // スムーススクロールのスピード
  var smooth_scroll_speed = 1000;

  // ページトップ出現位置
  var pagetop_view_pos = 100;


  // header
  var header_selector = '#global-head';
  var menu_btn_selector = '#menu-btn';

  // global-navi
  var gnavi_selector = '#global-nav';

  // page-top
  var pagetop_selector = '#page-top';



  /*---------------------------------------------------------------------------------------------------------------------
   *
   * ここから下は基本的に触らない
   *
   ---------------------------------------------------------------------------------------------------------------------*/
  var header = $(header_selector);
  var menu_btn = $(menu_btn_selector);
  var gnavi = $(gnavi_selector);
  var page_top = $(pagetop_selector);



  /*---------------------------------------
   * ページトップリンク動作
   ---------------------------------------*/
  pagetopAction();

  $(window).scroll(function(){
    pagetopAction();
  });

  function pagetopAction() {
    var scroll_top = $(window).scrollTop();
    if(scroll_top > pagetop_view_pos) {
      page_top.fadeIn(500);
    } else {
      page_top.fadeOut(500);
    }
  }


  /*---------------------------------------
   * スムーススクロール
   ---------------------------------------*/
  $('a[href*="#"], area[href*="#"]').not('.noScroll').click(function() {
    smoothScroll($(this));
    return false;
  });

  function smoothScroll(link) {
    var href = link.prop('href');
    var href_page_url = href.split('#')[0];
    var current_url = location.href;
    current_url = current_url.split('#')[0];

    if(href_page_url == current_url) {
      //リンク先の#からあとの値を取得
      href = href.split("#");
      href = href.pop();
      href = "#" + href;

      //スムースクロールの実装
      var target = $(href == "#" || href == "" ? 'html' : href);
      var scroll_position = target.offset().top; //targetの位置を取得
      $('body,html').animate({
        scrollTop:scroll_position
      }, smooth_scroll_speed, 'swing');
    } else {
      location.href = href;
    }
    return false;
  }


  /*---------------------------------------
   * Spメニュー
   ---------------------------------------*/
  // Gナビ プルダウン
  function gnaviToggle() {
    if(responsiveCheck('sp')) {
      gnavi.animate(
        {height: 'toggle'}, '300'
      );
      menu_btn.toggleClass('active');
    }
  }

  menu_btn.click(function() {
    gnaviToggle();
  });

});
