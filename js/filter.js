$(function () {
  $(document).ready(function() {
    // リストのCSS displayプロパティを取得
    default_display = $('.shop-list .list-item').css('display');

    $('.sort-nav li').click(function(){
      // メニューに.currentを付与
      $('.sort-nav li').removeClass('current');
      $(this).addClass('current');

      if($(this).hasClass('all')) { // すべてを選択した場合
        $('.shop-list .list-item').each(function() {
          $(this).css('display', default_display);
        });
      } else { // フィルターを実行
        $('.shop-list .list-item').css('display', 'none');
        filter_cat = ($(this).data('json')).cat;

        $('.shop-list .list-item').each(function() {
          this_cat = ($(this).data('json')).cat;
          if(this_cat == filter_cat) {
            $(this).css('display', default_display);
          }
        });
      }

      // masonryを再実行
      remasonry();
    });

    function remasonry() {
      $('.shop-list').masonry({
        itemSelector: '.list-item',
        columnWidth: 150,
        isFitWidth: true,
        isAnimated: true
      });
    }
  });
});
