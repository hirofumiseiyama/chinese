<!DOCTYPE html>
<html>

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!--og-->
  <meta property="og:title" content="用手機感應！「北海道東部釧路輪盤挑戰接力賽」">
  <meta property="og:type" content="website">
  <meta property="og:description" content="用手機感應！「北海道東部釧路輪盤挑戰接力賽」">
  <meta property="og:url" content="https://example.com/">
  <meta property="og:site_name" content="用手機感應！「北海道東部釧路輪盤挑戰接力賽」">

  <meta charset="UTF-8">
  <title>用手機感應！「北海道東部釧路輪盤挑戰接力賽」</title>
  <meta name="keywords" content="用手機感應！「北海道東部釧路輪盤挑戰接力賽」">
  <meta name="description" content="用手機感應！「北海道東部釧路輪盤挑戰接力賽」">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <link rel="canonical" href="https://example.com/">
  <link rel="stylesheet" href="css/main.css" />
  <link rel="apple-touch-icon" href="img/common/apple.png">

  <!--image max5-->
  <meta property="og:image" content="https://example.com/img/common/OG.png">

  <!--Twitter-->
  <meta name="twitter:card" content="summary_large_image">

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css">
  <script src="js/common.js"></script>
</head>

<body class="top">
  <header id="global-head">
    <div class="header-text">
      <div class="container">
        <p>再挑戰〇處即可參加終極輪盤</p>
      </div>
    </div>
    <h1><a href="index.php"><img src="img/common/main-logo01.png" alt="用手機感應！「北海道東部釧路輪盤挑戰接力賽」"></a></h1>
    <div class="container">
      <div class="keihin">
        <h2><img src="img/common/header-text01.png" alt="獎品一覽表"></h2>
        <div class="list">
          <dl>
            <dt class="num01">頭獎</dt>
            <dd>10000pts</dd>
            <dt class="num02">貳獎</dt>
            <dd>3000pts</dd>
            <dt class="num03">参獎</dt>
            <dd>1000pts</dd>
            <dt class="num04">肆獎</dt>
            <dd>200pts</dd>
          </dl>
        </div>
      </div>
      <div class="btn"><a href="challenge_point.php" class="btn01 green">輪盤挑戰地點請見此
</a></div>
    </div>
  </header>

  <main>
    <div class="container">
      <div class="top-content01">
        <ul class="menu">
          <li><a href="#modal-rule" class="noScroll" rel="modal:open"><img src="img/top/menu01.png" alt="規則說明"></a></li>
          <li><a href="#modal-notes" class="noScroll" rel="modal:open"><img src="img/top/menu02.png" alt="注意事項"></a></li>
          <li><a href="#modal-nfc" class="noScroll" rel="modal:open"><img src="img/top/menu03.png" alt="NFC"></a></li>
        </ul>
        <div class="btn"><a href="#modal-peach-point" class="btn01 noScroll" rel="modal:open">兌換Peach點數</a></div>
      </div>

      <section class="top-content02">
        <h2 class="title01"><span>近期兌獎紀錄</span></h2>
        <ul class="point-list">
          <li>
            <figure><img src="img/top/point04_top.svg" alt="肆獎 200pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point04_top.svg" alt="肆獎 200pts"></figure>
            <div class="point">
              <h3>釧路観光クルーズ船　SEA CRANE</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point03_top.svg" alt="参獎 1,000pts"></figure>
            <div class="point">
              <h3>釧路市動物園</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">1000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point02_top.svg" alt="貳獎 3,000pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">3000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point01_top.svg" alt="頭獎 10,000pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">10000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point03_top.svg" alt="参獎 1,000pts"></figure>
            <div class="point">
              <h3>釧路観光クルーズ船　SEA CRANE</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">1000</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point04_top.svg" alt="肆獎 200pts"></figure>
            <div class="point">
              <h3>釧路市動物園</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
          <li>
            <figure><img src="img/top/point04_top.svg" alt="肆獎 200pts"></figure>
            <div class="point">
              <h3>釧路フィッシャーマンズワーフMOO</h3>
              <p class="date">2019-06-28 14:35</p>
              <p class="get">200</p>
            </div>
          </li>
        </ul>
      </section>
    </div>

    <?php /* モーダル確認用 */?>
    <div class="text-center" style="margin-top: 150px;">
      <div class="container">
        <h2 class="title01"><span>モーダル表示確認</span></h2>
        <p><a href="#modal-already-touch" class="noScroll btn01" rel="modal:open">タッチ済み</a></p>
        <p><a href="#modal-form" class="noScroll btn01" rel="modal:open">入力フォーム</a></p>
        <p><a href="#modal-form-confirm" class="noScroll btn01" rel="modal:open">入力確認</a></p>
        <p><a href="#modal-form-thanks" class="noScroll btn01" rel="modal:open">ありがとうございました</a></p>
      </div>
    </div>
  </main>

  <footer id="global-foot">
    <nav>
      <ul class="container">
        <li><a href="index.php">TOP</a></li>
        <li><a href="challenge_point.php">輪盤挑戰地點</a></li>
      </ul>
    </nav>
    <div class="container">
      <p class="privacy"><a href="#">プライバシーポリシー</a></p>
      <small class="copylight">Copyright(C) 2019 Peach Aviation 株式会社</small>
    </div>
  </footer>

  <div id="page-top"><a href="#global-head"><img src="img/common/page-top.svg" alt="Page Top"></a></div>


<?php // ----------
// モーダルウィンドウ
// ---------------- ?>
<?php // modal#modal-rule?>
<div id="modal-rule" class="modal">
  <h2 class="title01"><span>規則說明</span></h2>
  <p>
    在北海道東部各處的輪盤挑戰地點使用智慧型手機感應，或是讀取QR碼。
  </p>
  <p>
    ▼
  </p>
  <p>
    參加輪盤挑戰賽（一個地方只能玩一次）。
  </p>
  <p>
    ▼
  </p>
  <p>
    有機會獲得樂桃航空的Peach點數。
  </p>
  <p>
    ▼
  </p>
  <p>
    可以在釧路機場的兌換處領取獎品。（兌換獎品時需填寫姓名及電子郵件信箱）
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントに引き換えのお申込みフォームが開く
  </p>
  <p>
    ▼
  </p>
  <p>
    姓名、会員情報を入力する
  </p>
  <p>
    ▼
  </p>
  <p>
    之後將會贈予中獎的Peach點數。
  </p>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p></div>
</div>

<?php // modal#modal-notes?>
<div id="modal-notes" class="modal">
  <h2 class="title01"><span>注意事項</span></h2>
  <p>
    參加活動需要可以使用NFC或是讀取QR碼功能的智慧型手機。
  </p>
  <p>
    若在兌換獎品前更換手機機型，所累積的獎品將會全數失效。
  </p>
  <p>
    請注意，若在活動期間內將集章網頁瀏覽器的cookie清除，所累積的獎品將會全數失效。
  </p>
  <p>
    不需下載專用的APP，即可參加活動。請注意本活動不支援摺疊手機。請使用智慧型手機（iPhone或Android)。
  </p>
  <p>
    參加本活動時，請使用標準瀏覽器開啟cookie功能（推薦iPhone使用Safari、Android系統使用Chrome）。若使用上述標準瀏覽器以外的OS系統，有可能會發生錯誤。
  </p>
  <p>
    在開啟隱私瀏覽模式的狀態下將無法收集獎品。
  </p>
  <p>
    請使用同一個網頁瀏覽器收集獎品。如果中途變更網頁瀏覽器，將無法收集獎品。
  </p>
  <p>
    請每次都使用同樣的APP讀取QR碼。
  </p>
  <p>
    連結獎品網站或參加兌換活動所使用的網路流量費用均由客戶負擔。
  </p>
  <p>
    若有安裝防毒APP，有可能會發生錯誤。當發生錯誤時，請關閉防毒APP，再試試看。
  </p>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p></div>
</div>

<?php // modal#modal-nfc?>
<div id="modal-nfc" class="modal">
  <h2 class="title01"><span>關於NFC</span></h2>
  <p>
    NFC是指只要用智慧型手機感應IC晶片，就會自動連結至網站的技術。此輪盤挑戰接力賽使用的就是NFC的功能。
  </p>
  <p>
    Android智慧型手機的用戶，請在解鎖後在主畫面感應。iPhoneXS、XS MAX、XR的用戶，不用解鎖就可以直接用NFC。
  </p>
  <p>
    iPhone7,8,9,X的用戶，可藉由NFC Reader App使用NFC。（需要另外下載）
  </p>
  <p>
    若發生感應不良的情形，請嘗試以下方法。
  </p>
  <p>
    有的時候感應需要花較長的時間，請感應久一點。
  </p>
  <p>
    Android智慧型手機用戶，麻煩確認是否開啟NFC功能。
  </p>
  <p>
    有可能是NFC感應器感應不到紙卡，請調整感應的位置。（iPhone的感應器位於手機的上方）
  </p>
  <p>
    若還是無法解決，請改用掃描QR碼。
  </p>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p></div>
</div>

<?php // modal#modal-peach-point?>
<div id="modal-peach-point" class="modal">
  <h2 class="title01"><span>引き換え方法</span></h2>
  <p>
    引換所（空港の観光カウンター）にて、引き換え用スマプレを読み込こむ
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントに引き換えのお申込みフォームが開く
  </p>
  <p>
    ▼
  </p>
  <p>
    姓名、会員情報を入力する
  </p>
  <p>
    ▼
  </p>
  <p>
    ピーチポイントが付与される
  </p><br>
  <div><figure class="text-center"><img src="img/top/map.png" alt=""></figure></div>
  <div class="close-btn01"><p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p></div>
</div>

<?php // modal#modal-already-touch?>
<div id="modal-already-touch" class="modal">
  <p class="text-large text-center">
    已完成感應。
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p></div>
</div>

<?php // modal#modal-form?>
<div id="modal-form" class="modal">
  <form action="#" method="post">
    <p><label><input type="radio" name="member" value="会員">会員</label></p>
    <p><label><input type="radio" name="member" value="非会員">非会員</label></p>
    <p><input type="text" name="your-name" placeholder="姓名"></p>
    <p><input type="email" name="your-mail" placeholder="電子郵件信箱"></p>
    <div class="close-btn02">
      <p><input type="submit" value="下一步" class="btn01 green"></p>
      <p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p>
    </div>
  </form>
</div>

<?php // modal#modal-form-confirm?>
<div id="modal-form-confirm" class="modal">
  <form action="#" method="post">
    <p class="text-large text-center">
      入力した姓名
    </p>
    <p class="text-large text-center">
      入力した電子郵件信箱
    </p>
    <div class="close-btn02">
      <p><input type="submit" value="送出" class="btn01 green"></p>
      <p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p>
    </div>
  </form>
</div>

<?php // modal#modal-form-thanks?>
<div id="modal-form-thanks" class="modal">
  <p class="text-large text-center">
    感謝您參加本次活動
  </p>
  <figure class="text-center"><img src="img/top/img01.png" alt=""></figure>
  <p class="text-large text-center">
    歡迎繼續挑戰北海道東部釧路輪盤挑戰接力賽！
  </p>
  <div class="close-btn02"><p><a href="#close-modal" rel="modal:close" class="btn01 green">關閉頁面</a></p></div>
</div>

</body>

</html>